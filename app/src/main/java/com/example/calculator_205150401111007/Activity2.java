package com.example.calculator_205150401111007;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        TextView textviewHasil = (TextView) findViewById(R.id.textView10);
        TextView textviewOperasi = (TextView) findViewById(R.id.textView3);
        Intent intent = getIntent();
        String nilai = intent.getStringExtra("hasil");
        String operasi = intent.getStringExtra("operasi");
        textviewOperasi.setText(operasi);
        textviewHasil.setText(nilai);
    }
}