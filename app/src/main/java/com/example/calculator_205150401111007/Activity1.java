package com.example.calculator_205150401111007;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Activity1 extends AppCompatActivity implements View.OnClickListener {

    TextView textview;
    Button btn1, btn2, btn3, btn4;
    Button btn5, btn6, btn7, btn8, btn9, btn0;
    Button btn10, btnPlus, btnMin, btnPow, btnPer, btnRes;
    int num, opr;
    Intent intent;
    String operasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
        textview = (TextView) findViewById(R.id.textView5);
        num = 0; opr = 0; operasi = "";
        btn1 = (Button) findViewById(R.id.button_1);
        btn2 = (Button) findViewById(R.id.button2);
        btn3 = (Button) findViewById(R.id.button3);
        btn4 = (Button) findViewById(R.id.button4);
        btn5 = (Button) findViewById(R.id.button5);
        btn6 = (Button) findViewById(R.id.button6);
        btn7 = (Button) findViewById(R.id.button7);
        btn8 = (Button) findViewById(R.id.button8);
        btn9 = (Button) findViewById(R.id.button9);
        btn10 = (Button) findViewById(R.id.button10);
        btn0 = (Button) findViewById(R.id.button11);
        btnPlus = (Button) findViewById(R.id.button_);
        btnMin = (Button) findViewById(R.id.button_2);
        btnPow = (Button) findViewById(R.id.button_3);
        btnPer = (Button) findViewById(R.id.button_4);
        btnRes = (Button) findViewById(R.id.button12);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btn10.setOnClickListener(this);
        btn0.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnMin.setOnClickListener(this);
        btnPow.setOnClickListener(this);
        btnPer.setOnClickListener(this);
        btnRes.setOnClickListener(this);
        intent = new Intent(this, Activity2.class);
    }

    void calculate(){
        int num2 = Integer.parseInt(textview.getText().toString());
        if (opr == 1) {
            num += num2;
        } else if (opr == 2) {
            num -= num2;
        } else if (opr == 3) {
            num *= num2;
        } else if (opr == 4) {
            num /= num2;
        }
    }

    @Override
    public void onClick(View view) {
        boolean cek = textview.getText().toString().equals("0");
        if(view.getId() == R.id.button12) {
            operasi += textview.getText();
            calculate();
            intent.putExtra("operasi", operasi);
            intent.putExtra("hasil", String.valueOf(num));
            startActivity(intent);
//            textview.setText(String.valueOf(num));
            num = 0; operasi = "";
            textview.setText("0");
        }else if(view.getId() == R.id.button_) {
            operasi += textview.getText() + "+";
            if(num != 0) {
                calculate();
                textview.setText(String.valueOf(num));
            }
            num = Integer.parseInt(textview.getText().toString());
            textview.setText("0");
            opr = 1;
        }else if(view.getId() == R.id.button_2) {
            operasi += textview.getText() + "-";
            if(num != 0) {
                calculate();
                textview.setText(String.valueOf(num));
            }
            num = Integer.parseInt(textview.getText().toString());
            textview.setText("0");
            opr = 2;
        }else if(view.getId() == R.id.button_3) {
            operasi += textview.getText() + "*";
            if(num != 0) {
                calculate();
                textview.setText(String.valueOf(num));
            }
            num = Integer.parseInt(textview.getText().toString());
            textview.setText("0");
            opr = 3;
        }else if(view.getId() == R.id.button_4) {
            operasi += textview.getText() + "/";
            if(num != 0) {
                calculate();
                textview.setText(String.valueOf(num));
            }
            num = Integer.parseInt(textview.getText().toString());
            textview.setText("0");
            opr = 4;
    }else if(view.getId() == R.id.button10) {
            textview.setText("0");
            num = 0;
        }else{
            if(view.getId() == R.id.button_1){
                if(cek) textview.setText("1");
                else textview.setText(textview.getText() + "1");
            }else if(view.getId() == R.id.button2){
                if(cek) textview.setText("2");
                else textview.setText(textview.getText() + "2");
            }else if(view.getId() == R.id.button3){
                if(cek) textview.setText("3");
                else textview.setText(textview.getText() + "3");
            }else if(view.getId() == R.id.button4){
                if(cek) textview.setText("4");
                else textview.setText(textview.getText() + "4");
            }else if(view.getId() == R.id.button5){
                if(cek) textview.setText("5");
                else textview.setText(textview.getText() + "5");
            }else if(view.getId() == R.id.button6){
                if(cek) textview.setText("6");
                else textview.setText(textview.getText() + "6");
            }else if(view.getId() == R.id.button7){
                if(cek) textview.setText("7");
                else textview.setText(textview.getText() + "7");
            }else if(view.getId() == R.id.button8){
                if(cek) textview.setText("8");
                else textview.setText(textview.getText() + "8");
            }else if(view.getId() == R.id.button9) {
                if(cek) textview.setText("9");
                else textview.setText(textview.getText() + "9");
            }else if(view.getId() == R.id.button11) {
                textview.setText(textview.getText() + "0");
            }
        }
    }
}